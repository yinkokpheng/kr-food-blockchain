# project namespace
DOMAIN=blockchain.nipa

# orderer domain name
ORDERER=orderer.$DOMAIN

# organizations domain name
ORGANIZATIONS_DOMAIN_NAME=(school.$DOMAIN food.$DOMAIN agri.$DOMAIN city.$DOMAIN)

# organization msp
ORGANIZATIONS_MSP=(SchoolMSP FoodMSP AgriMSP CityMSP)

# organization name
ORGANIZATIONS_NAME=(school food agri city)

# Count Number of ORG
NUMBER_OF_ORG=${#ORGANIZATIONS_NAME[@]}

# channel name defaults to "mychannel"
export CHANNEL_NAME="mychannel"

# configtx.yaml orderer genesis profile
export ORDERER_GENESIS_PROFILE=FoodSafetyOrgsOrdererGenesis

# configtx.yaml channel profile
export CHANNEL_PROFILE=FoodSafetyOrgsChannel

# use this as the default docker-compose yaml definition
COMPOSE_FILE=docker-compose-cli.yaml
COMPOSE_FILE_COUCH=docker-compose-couch.yaml
COMPOSE_FILE_CAS=docker-compose-cas.yaml

LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
CC_SRC_PATH="github.com/chaincode/chaincode_example/go/"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example/node/"
fi

if [ "$LANGUAGE" = "java" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example/java/"
fi