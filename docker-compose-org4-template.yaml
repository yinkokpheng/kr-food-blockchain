# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

version: "3.4"

volumes:
  peer0.city.blockchain.nipa:

networks:
  kfbn:
    external:
      name: $SWARM_NETWORK

services:
  ca-city:
    image: hyperledger/fabric-ca:$IMAGE_TAG
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=ca-city
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.city.blockchain.nipa-cert.pem
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.city.blockchain.nipa-cert.pem
      - FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/CA4_PRIVATE_KEY
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server-config/CA4_PRIVATE_KEY
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/ca.city.blockchain.nipa-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/CA4_PRIVATE_KEY -b admin:adminpw -d'
    volumes:
      - ./crypto-config/peerOrganizations/city.blockchain.nipa/ca/:/etc/hyperledger/fabric-ca-server-config
    container_name: ca-city
    deploy:
      mode: replicated
      replicas: 1
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.hostname == $PC2_HOSTNAME
    ports:
      - published: 8054
        target: 7054
        mode: host
    networks:
      kfbn:
        aliases:
          - ca.city.blockchain.nipa

  couchdb-city:
    container_name: couchdb-city
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    deploy:
      mode: replicated
      replicas: 1
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.hostname == $PC2_HOSTNAME
    ports:
      - published: 6984
        target: 5984
        mode: host
    networks:
      kfbn:
        aliases:
          - couchdb.city.blockchain.nipa

  peer0-city:
    container_name: peer0.city.blockchain.nipa
    image: hyperledger/fabric-peer:$IMAGE_TAG
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_network
      #- CORE_LOGGING_LEVEL=INFO
      - CORE_LOGGING_LEVEL=DEBUG
      - CORE_CHAINCODE_STARTUPTIMEOUT=1200s
      - CORE_CHAINCODE_LOGGING_LEVEL=DEBUG
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:8052
      - CORE_PEER_ID=peer0.city.blockchain.nipa
      - CORE_PEER_ADDRESS=peer0.city.blockchain.nipa:8051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.city.blockchain.nipa:8051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.city.blockchain.nipa:8051
      - CORE_PEER_LOCALMSPID=CityMSP
      # Couch DB
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb-city:5984
      # The CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME and CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
      # provide the credentials for ledger to connect to CouchDB.  The username and password must
      # match the username and password set for the associated CouchDB.
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/city.blockchain.nipa/peers/peer0.city.blockchain.nipa/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/city.blockchain.nipa/peers/peer0.city.blockchain.nipa/tls:/etc/hyperledger/fabric/tls
      #- ./crypto-config/peerOrganizations/city.blockchain.nipa/peers/peer0.city.blockchain.nipa/ca:/etc/hyperledger/fabric/ca
      - peer0.city.blockchain.nipa:/var/hyperledger/production
    deploy:
      mode: replicated
      replicas: 1
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.hostname == $PC2_HOSTNAME
    ports:
      - published: 8051
        target: 7051
        mode: host
      - published: 8053
        target: 7053
        mode: host
    networks:
      kfbn:
        aliases:
          - peer0.city.blockchain.nipa

  cli-city:
    container_name: cli-city
    image: hyperledger/fabric-tools:latest
    tty: true
    stdin_open: true
    deploy:
      mode: replicated
      replicas: 1
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.hostname == $PC2_HOSTNAME
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_LOGGING_LEVEL=DEBUG
      #- CORE_LOGGING_LEVEL=INFO
      - CORE_PEER_ID=cli-city
      - CORE_PEER_ADDRESS=peer0.city.blockchain.nipa:8051
      - CORE_PEER_LOCALMSPID=CityMSP
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/city.blockchain.nipa/peers/peer0.city.blockchain.nipa/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/city.blockchain.nipa/peers/peer0.city.blockchain.nipa/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/city.blockchain.nipa/peers/peer0.city.blockchain.nipa/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/city.blockchain.nipa/users/Admin@city.blockchain.nipa/msp
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ./chaincode/:/opt/gopath/src/github.com/chaincode
      - ./crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ./scripts:/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/
      - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    networks:
      - kfbn
